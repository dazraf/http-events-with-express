const express = require("express");
const app = express();
const port = 3000;

app.use(express.static("public"));

app.get("/listen", (req, res) => {
  res.header("transfer-encoding", "chunked");
  res.set("Content-Type", "text/json");

  var callback = function (data) {
    console.log("data");
    res.write(JSON.stringify(data));
  };

  const timerId = setInterval(() => {
    callback({ mydata: "data" });
  }, 1000);

  res.socket.on("end", function () {
    clearInterval(timerId);
  });
});

app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
